const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  devServer:{proxy: 'https://nl227f95td.execute-api.us-east-1.amazonaws.com/dpl/'},
  transpileDependencies: true,
  pluginOptions: {
    vuetify: {
			// https://github.com/vuetifyjs/vuetify-loader/tree/next/packages/vuetify-loader
		}
  }
})
