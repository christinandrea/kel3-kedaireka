export const datatren = {
    type: "bar",
    data: {
        labels: null,
        datasets: [
            {
                label: "Karbon Tanaman",
                data: null,
                borderColor: "#7CB342",
                backgroundColor: "rgba(56, 255, 58, 0.2)",
                borderWidth: 3,
                fill: false
            },
            {
                label: "Karbon Tanah",
                data: null,
                borderColor: "#00B0FF",
                backgroundColor: "rgba(56, 120, 255, 0.2)",
                borderWidth: 3,
                fill: false
            }
        ]
    },
    options: {
        responsive: true,
        lineTension: 1,
        scales: {
            yAxes: [
                {
                    ticks: {
                        beginAtZero: true,
                        padding: 25
                    }
                }
            ]
        }
    }
};

export default datatren;