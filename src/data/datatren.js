export const datatren = {
    type: "line",
    data: {
        labels: null,
        datasets: [
            {
                label: "Karbon Tanaman",
                data: null,
                borderColor: "#7CB342",
                backgroundColor: "rgba(56, 255, 58, 0.2)",
                borderWidth: 3,
                fill: 1,
                tension: 0,
            },
            {
                label: "Karbon Tanah",
                data: null,
                borderColor: "#00B0FF",
                backgroundColor: "rgba(56, 120, 255, 0.2)",
                borderWidth: 3,
                fill: "origin",
                tension: 0,
            }
        ]
    },
    options: {
        responsive: true,
        lineTension: 1,
        scales: {
            yAxes: [
                {
                    ticks: {
                        beginAtZero: true,
                        padding: 15,
                    }
                }
            ]
        }
    }
};

export default datatren;